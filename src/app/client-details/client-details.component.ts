import { Component, OnInit  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { SclientService } from '../sclient.service';
import { IClient } from '../clientIntr';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})

export class ClientDetailsComponent implements OnInit {

  client?: IClient;

  constructor(
    private route : ActivatedRoute,
    private locaiton : Location,
    private clientSer : SclientService
  ) { }

  ngOnInit(): void {
    this.getClient();
  }

  getClient(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id')!, 10);
      this.clientSer.getClient(id)
        .subscribe(client => this.client = client);
  }

  goBack(){
    this.locaiton.back();
  }

  /*
  save(): void {
    if (this.client) {
      this.clientSer.updateClient(this.client)
        .subscribe(() => this.goBack());
    }
  }
  */

}
