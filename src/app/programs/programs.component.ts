import { Component, OnInit } from '@angular/core';
import { Program } from '../programIntr';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  styleUrls: ['./programs.component.css']
})
export class ProgramsComponent implements OnInit {

  program: Program = {
    type: 'Bulk',
    name: 'Beast',
    length: 6
  }

  constructor() { }

  ngOnInit(): void {
  }

}
