export interface IClient{
    id : number;
    name: string;
    program_name: string;
    age: number;
    password: string;
    isAdmin: boolean;
}