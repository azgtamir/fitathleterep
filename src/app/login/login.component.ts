import { Component, OnInit } from '@angular/core';
import { IClient } from '../clientIntr';
import { SclientService } from '../sclient.service';
import { LoginStatusService } from '../login-status.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  name ?: string;
  id ?: string;
  password ?: string;
  client ?: IClient;

  constructor(private clientSer : SclientService,
              private loginSer : LoginStatusService,
              private router : Router) { }

  ngOnInit(): void {
  }

  login() : void {
    if(this.id != undefined){
      this.clientSer.getClient(Number(this.id))
                        .subscribe(parClient =>{
                           this.client = parClient
                           // delete this.client[0]._id;
                           if(this.client != undefined){
                            if(this.client.password === this.password){
                              this.loginSer.loginStatus = true;
                              this.router.navigate(['/home']);
                              if(this.client.isAdmin){
                                this.loginSer.isAdmin = true;
                              } 
                            }
                          }
                        });
    }
  }

}
