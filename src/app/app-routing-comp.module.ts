import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClientComponent } from './client/client.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClientDetailsComponent } from './client-details/client-details.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SinginComponent } from './singin/singin.component';

/*import { ClientDetailsComponent } from './client-details/client-details.component';
import { MessagesComponent } from './messages/messages.component';
import { ProgramsComponent } from './programs/programs.component';*/

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'clients', component: ClientComponent },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'details/:id', component: ClientDetailsComponent},
  { path: 'home', component: HomeComponent},
  { path: 'login', component: LoginComponent },
  { path: 'signin', component: SinginComponent}
  /*{ path: 'clients-details', component: ClientDetailsComponent },
  { path: 'messages', component: MessagesComponent },
  { path: 'program', component: ProgramsComponent },*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
