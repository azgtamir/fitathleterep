import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

import { IClient } from '../clientIntr';
import { SclientService } from '../sclient.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {

  client ?: IClient;
  @Input() ClientList: IClient[] = [];

  constructor(private clientSer : SclientService) { }

  ngOnInit(): void {
  }

  /*add(id : string, name: string, program_name: string, age: string, password: string, isAdmin: string): void {
    name = name.trim();
    id = id.trim();
    program_name = program_name.trim();
    age = age.trim();
    password = password.trim();
    isAdmin = isAdmin.trim();
    
    if (!name) { return; }
    this.clientSer.addClient({ name } as IClient)
      .subscribe(client => {
        this.ClientList.push(client);
      });
  }*/

}
