import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { IClient } from './clientIntr';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService{

  createDb(){
    const MEM_CLIENT_LIST =  [
      {id : 1, name: "Tamir", program_name: "Beast", age: 17, password: "a123", isAdmin: true},
      {id : 2, name: "Ron", program_name: "Beast", age: 24, password: "a123", isAdmin: false},
      {id : 3, name: "Hadar", program_name: "Fozen", age: 23, password: "a123", isAdmin: false},
      {id : 4, name: "Nizan", program_name: "Fozen", age: 22, password: "a123", isAdmin: false},
      {id : 5, name: "Dad", program_name: "Daddy", age: 50,password: "a123", isAdmin: false}
    ];
    return {MEM_CLIENT_LIST};
  }

  genId(clients: IClient[]): number {
    return clients.length > 0 ? Math.max(...clients.map(client => client.id)) + 1 : 11;
  }
}
