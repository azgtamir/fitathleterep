import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { IClient } from './clientIntr';
import { MessageService } from './message.service';
import { LoginStatusService } from './login-status.service';

 
@Injectable({
  providedIn: 'root'
})
export class SclientService {

  /*getClients() : IClient[] {
    return CLIENT_LIST;
  }
  private clientsUrl = 'api/MEM_CLIENT_LIST';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  */

  constructor(
    private http: HttpClient,
    private messageService : MessageService,
    private loginSer : LoginStatusService) 
  { }

  getClients() : Observable<IClient[]>{
    return this.http.get<IClient[]>('/api/getAllClients/')
  }

  getClient(id: number) : Observable<IClient> {
    return this.http.get<IClient>('/api/getClientById/', {params:{"id":id}});
  }

  addClient(client : IClient){
    let body = JSON.stringify(client);
    // let body=JSON.stringify({id : 5, name: "Mon", program_name: "Mommy", age: 50, password: "a123", isAdmin: false});
    let headers = new HttpHeaders({ 'Content-Type': 'application/JSON' });

    return this.http.post('/api/postData', body, {headers : headers}); //-- JSON.parse(client)
  }  
  /*
  getClients(): Observable<IClient[]> {
    return this.http.get<IClient[]>(this.clientsUrl)
      .pipe(
        tap(_ => this.log('fetched clients')),
        catchError(this.handleError<IClient[]>('getClients', []))
      );
  }

  getClient(id: number): Observable<IClient> {
    const url = `${this.clientsUrl}/${id}`;
    return this.http.get<IClient>(url).pipe(
      tap(_ => this.log(`fetched client id=${id}`)),
      catchError(this.handleError<IClient>(`getClient id=${id}`))
    );
  }

  private log(message: string) {
    this.messageService.add(`clientService: ${message}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updateClient(client: IClient): Observable<any> {
    return this.http.put(this.clientsUrl, client, this.httpOptions).pipe(
      tap(_ => this.log(`updated client id=${client.id}`)),
      catchError(this.handleError<any>('updateClient'))
    );
  }

  addClient(client : IClient): Observable<IClient> {
    return this.http.post<IClient>(this.clientsUrl, client, this.httpOptions).pipe(
      tap((newClinet : IClient) => this.log(`added client w/ id=${newClinet.id}`)),
      catchError(this.handleError<IClient>('addClient'))
    );
  }
  */
}


