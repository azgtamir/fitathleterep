export interface Program{
    type: string;
    name: string;
    length: number;
}