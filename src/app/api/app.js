const express = require('express')

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://127.0.0.1:27017/";

const app = express()

// <----- Angular ------>

// require the body-parser
const bodyParser = require('body-parser')

app.get('/', (req, res) => {
    res.send('Welcome to Node API')

})


app.get('/getAllClients', (req, res) => {
    MongoClient.connect(url, function(err, db) {
        var dbo = db.db("myAthleteDB");
        if (err) throw err;
        dbo.collection("clientsdb").find({}).toArray(function(err, result)  {
          if (err) throw err;
          for(var i = 0; i < result.length; i++){
            delete result[i]._id;
            }
          res.json(result);
          db.close();
        });
    });
})

app.get('/getClientById', (req, res) => {
    MongoClient.connect(url, function(err, db) {
        var dbo = db.db("myAthleteDB");
        /*if (err) throw err;
        dbo.collection("clientsdb").find({}).toArray(function(err, result)  {
          if (err) throw err;
          res.json(result);
          db.close();
        });*/

        var query = { id : Number(req.query['id']) };
        dbo.collection("clientsdb").find(query).toArray(function(err, result)  {
            if (err) throw err;
            delete result[0]._id;
            res.json(result[0]);
            db.close();
        });
    });
})

app.post('/postData', bodyParser.json(), (req, res) => {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("myAthleteDB");
        // close the breach // 
        var myobj = req.body;
        dbo.collection("clientsdb").insertOne(myobj, function(err, result) {
          if (err) throw err;
          console.log("1 document inserted");
          res.send("1 document inserted");
          db.close();
        });
    });
})

app.listen(3000, () => console.log('Example app listening on port 3000!'))

// <----- Mongo ----->

