import { Component, OnInit } from '@angular/core';

import { SclientService } from '../sclient.service';
import { MessageService } from '../message.service';
import { IClient } from '../clientIntr';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  ClientList: IClient[] = [];
  
  constructor(private SerClient : SclientService, private SerMessage : MessageService) { }

  ngOnInit(): void {
    this.getClients();
  }

  getClients(): void{
    this.SerClient.getClients()
     .subscribe(kaka => this.ClientList = kaka)
  }

}


