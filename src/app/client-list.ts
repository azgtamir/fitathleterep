import { IClient } from "./clientIntr"

export const  CLIENT_LIST: IClient[] = [
    {id : 1, name: "Tamir", program_name: "Beast", age: 17, password: "a123", isAdmin: true},
    {id : 2, name: "Ron", program_name: "Beast", age: 24, password: "a123", isAdmin: false},
    {id : 3, name: "Hadar", program_name: "Fozen", age: 23, password: "a123", isAdmin: false},
    {id : 4, name: "Nizan", program_name: "Fozen", age: 22, password: "a123", isAdmin: false},
    {id : 5, name: "Dad", program_name: "Daddy", age: 50, password: "a123", isAdmin: false}
];