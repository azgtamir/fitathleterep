import { Component, OnInit } from '@angular/core';

import { IClient } from '../clientIntr';
import { SclientService } from '../sclient.service';
import { LoginStatusService } from '../login-status.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  clients: IClient[] = [];
  isAdmin : boolean = false;

  constructor(private heroService : SclientService,
              public loginSer : LoginStatusService) { }

  ngOnInit() {
    this.getClients();
  }

  getClients(): void {
    this.heroService.getClients()
      .subscribe(clients => this.clients = clients.slice(1, 5));
  }
}